package com.itb.charactersheet.screens.recyclerview;


import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;


import com.itb.charactersheet.R;
import com.itb.charactersheet.model.CharacterSheet;

import java.util.List;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class CharacterSheetListFragment extends Fragment {

    private CharacterSheetListViewModel mViewModel;
    private RecyclerView recyclerView;
    private CharacterSheetAdapter characterSheetAdapter;
    private Spinner spinnerOrder;
    private Spinner spinnerFilter;
    private EditText etFilter;
    private int filterPosition = 0;


    public static CharacterSheetListFragment newInstance() {
        return new CharacterSheetListFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.character_sheet_list_fragment, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this,view);

        recyclerView = view.findViewById(R.id.recyclerView);
        etFilter = view.findViewById(R.id.etFilter);

        recyclerView.setHasFixedSize(true);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);

        recyclerView.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = ViewModelProviders.of(this).get(CharacterSheetListViewModel.class);

        LiveData<List<CharacterSheet>> characterSheets = mViewModel.getAllCharacterSheets();
        characterSheetAdapter = new CharacterSheetAdapter();
        characterSheetAdapter.setOnCharacterSheetClickListener(this::onCharacterSheetClicked);
        recyclerView.setAdapter(characterSheetAdapter);
        characterSheets.observe(this, this::onCharacterSheetChanged);

        configureSpinnerOrder();
        configureSpinnerFilter();
    }

    private void configureSpinnerOrder(){
        spinnerOrder = (Spinner) getView().findViewById(R.id.spinnerOrder);
        String [] values = {"Default","Player","Character Name","Level"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), R.layout.my_spinner, values);
        adapter.setDropDownViewResource(R.layout.my_spinner);
        spinnerOrder.setAdapter(adapter);
        spinnerOrder.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getOrder(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getOrder(int position) {
        if (position == 0){
            LiveData<List<CharacterSheet>> characterSheet = mViewModel.getAllCharacterSheets();
            characterSheet.observe(this,this::onCharacterSheetChanged);
        }else if(position==1){
            LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsOrderedByPlayer();
            characterSheet.observe(this,this::onCharacterSheetChanged);
        } else if(position==2){
            LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsOrderedByName();
            characterSheet.observe(this,this::onCharacterSheetChanged);
        } else if(position==3){
            LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsOrderedByLevel();
            characterSheet.observe(this,this::onCharacterSheetChanged);
        }
    }

    private void configureSpinnerFilter(){
        spinnerFilter = (Spinner) getView().findViewById(R.id.spinnerFilter);
        String [] values = {"Default","Player","Character","Level","Level <","Level >"};
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(), R.layout.my_spinner, values);
        adapter.setDropDownViewResource(R.layout.my_spinner);
        spinnerFilter.setAdapter(adapter);
        spinnerFilter.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                getFilter(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void getFilter(int position) {
        filterPosition=position;
        if (position == 0){
            LiveData<List<CharacterSheet>> characterSheet = mViewModel.getAllCharacterSheets();
            characterSheet.observe(this,this::onCharacterSheetChanged);
        }else if(position==1){
            setEditText("Player",InputType.TYPE_CLASS_TEXT);
        } else if(position==2){
            setEditText("Character",InputType.TYPE_CLASS_TEXT);
        } else if(position==3){
            setEditText("Level",InputType.TYPE_CLASS_NUMBER);
        } else if(position==4){
            setEditText("Level",InputType.TYPE_CLASS_NUMBER);
        } else if(position==5){
            setEditText("Level",InputType.TYPE_CLASS_NUMBER);
        }
    }

    private void setEditText(String hint, int type){
        etFilter.setHint(hint);
        etFilter.setInputType(type);
    }

    @OnClick(R.id.btnFilter)
    public void onBtnFilterClicked(){
        String textFilter = etFilter.getText().toString().trim();
        if(textFilter.isEmpty()){
            etFilter.setError("Nothing to filter");
        } else {
            if (filterPosition == 0) {
                etFilter.setError("You can't filter by defaults");
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getAllCharacterSheets();
                characterSheet.observe(this, this::onCharacterSheetChanged);
            } else if (filterPosition == 1) {
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsByPlayer(textFilter);
                characterSheet.observe(this, this::onCharacterSheetChanged);
            } else if (filterPosition == 2) {
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsByName(textFilter);
                characterSheet.observe(this, this::onCharacterSheetChanged);
            } else if (filterPosition == 3) {
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsByExactLevel(Integer.valueOf(textFilter));
                characterSheet.observe(this, this::onCharacterSheetChanged);
            } else if (filterPosition == 4) {
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsByLowerLevel(Integer.valueOf(textFilter));
                characterSheet.observe(this, this::onCharacterSheetChanged);
            } else if (filterPosition == 5) {
                LiveData<List<CharacterSheet>> characterSheet = mViewModel.getCharacterSheetsByGreaterLevel(Integer.valueOf(textFilter));
                characterSheet.observe(this, this::onCharacterSheetChanged);
            }
        }
    }

    private void onCharacterSheetChanged(List<CharacterSheet> characterSheets) {
        characterSheetAdapter.setCharacterSheetList(characterSheets);
    }

    private void onCharacterSheetClicked(CharacterSheet characterSheet, View view) {
        NavDirections action = CharacterSheetListFragmentDirections.actionDetails(characterSheet.getId());
        Navigation.findNavController(view).navigate(action);
    }

    @OnClick(R.id.fabAdd)
    public void onButtonAddClicked(){
        Navigation.findNavController(recyclerView).navigate(R.id.action_create);
    }



}
