Trello: https://trello.com/b/XOUnNXlX/charactersheet

Hemos desarrollado los 7 items que se pedian en la práctica:
- Una vista de creación de ficha de personaje
- Una vista para ver los detalles de la ficha de personaje
- Un listado de todas las fichas creadas
- La funcionalidad de poder editar las fichas
- La funcionalidad de poder eliminar las fichas
- Hemos introducido nuevos campos y los hemos migrado a la base de datos
- La funcionalidad de ordenar las fichas de personaje según jugador, personaje o nivel
- La funcionalidad de filtrar las fichas de personaje según jugador, personaje, nivel exacto, nivel inferior o nivel superior a tal (No se puede filtar en default y no se puede filtrar sin rellenar el campo, se ha de poner el valor exacto para filtrar)


Extras:
- La funcionalidad de cambiar a modo nocturno / diurno
- Splash Screen al inicio de la aplicación
- Icono de la aplicación
- Confirmación para salir de la aplicación con doble click al botón de atrás
- La funcionalidad de feedback para enviar un correo
- La funcionalidad de tener un máximo de puntos por nivel para gastar en ciertos atributos del personaje (Cada nivel es un punto, si gastas más puntos que nivel te rellena los atributos a 0 y te avisa con un Toast)
